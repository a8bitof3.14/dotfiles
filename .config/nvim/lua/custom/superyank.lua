-- Superyank: Copy the entire contents of the buffer into the system clipboard.

local function get_pos()
	vim.cmd('let g:pos = getpos(".")')
	local pos = vim.g.pos
	vim.cmd('unlet g:pos')
	return pos
end

local function set_pos(pos)
	vim.cmd(string.format('call setpos(".", [%d, %d, %d, %d])', pos[1], pos[2],
	pos[3], pos[4]))
end

function yank_entire_buffer()
	local saved_pos = get_pos()
	vim.cmd('norm gg')
	vim.cmd('norm "+yG')
	set_pos(saved_pos)
end

vim.api.nvim_set_keymap('', '<Leader>sy', ':lua yank_entire_buffer()<CR>', { noremap = true, silent = true })
