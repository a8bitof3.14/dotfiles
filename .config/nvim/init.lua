vim.g.mapleader = ' '
require('plugins')

vim.opt.exrc = true -- Source vimrc in current directory if there is one
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.hidden = true -- Switch from unsaved edited buffer
vim.opt.completeopt = {'menuone', 'noinsert', 'noselect'}
vim.opt.laststatus = 3 -- All splits use the same status line

-- require('theme/gruvbox')
require('custom/superyank')

require('plugins/telescope')
require('plugins/treesitter')
require('plugins/lsp')
