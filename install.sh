#!/bin/sh

# Downloads LARBS, deploys custom dotfiles and performs post-install actions.

larbs_src=https://raw.githubusercontent.com/LukeSmithxyz/LARBS/master/larbs.sh
dotfiles_url=https://gitlab.com/gabfelix/dotfiles
myprogs_url=https://gitlab.com/gabfelix/dotfiles/-/raw/master/progs.csv

function post_install
{
	clear
	echo "Cleaning up leftover files..."
	rm --recursive --force "$HOME/.git" "$HOME/larbs.sh" "$HOME/progs.csv" "$HOME/testing" "$HOME/LICENSE"
	echo "Creating directory structure for $HOME..."
	mkdir -p "$HOME/doc" "$HOME/vid" "$HOME/dl" "$HOME/img" "$HOME/mus" "$HOME/img/shots" "$HOME/.local/share/lyrics" "$HOME/.local/bin" "${XDG_CONFIG_HOME:-$HOME/.config}/mpd/playlists"
	echo "Installing NeoVim plugins..."
	nvim --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync' > /dev/null
	echo "Installing TreeSitter parsers in 5 seconds. Exit will be manual."
	sleep 5 && nvim +'TSInstall all'
	echo "Installation completed. Please exit and re-login to apply changes."
}

function die
{
	echo "$1"
	exit 1
}

sudo pacman -Syu || die "Could not update system."
sudo pacman -S --noconfirm git curl || die "Could not install dependencies."
curl -LO "$larbs_src" && \
chmod +x "./${larbs_src##*/}" && \
sudo ./${larbs_src##*/} -r "$dotfiles_url" -p "$myprogs_url" && \
post_install
