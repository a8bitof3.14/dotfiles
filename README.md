# dotfiles
This repository contains all my dotfiles and customizations. Many files are based on or entirely copied from [Luke Smith's dotfiles](https://github.com/lukesmithxyz/voidrice).

It comes with the `install.sh` script which downloads LARBS and uses it to install them. This script is the only thing necessary to install the dotfiles.

## Important
* Both the script and the files are intended for my personal use and may have quirks unique to my setup which other people may not want. If anyone decides to use them, be advised that I will not make any effort to remove any of them.
* The **only** target of the setup is **Arch Linux**. It is not intended for other distros (including Arch derivatives like Manjaro, Artix or Endeavour).
* The `install.sh` script will not perform any steps of the base install of Arch Linux. It is intended to be run after those steps have been completed.
* Some AUR packages will be installed. It is advisable to inspect the PKGBUILDs before installing. To do that, take a look at everything with the `A` tag in `progs.csv` before installing.

## Instructions
In a fresh install of Arch Linux, run:
```
curl -LO "https://gitlab.com/gabfelix/dotfiles/-/raw/master/install.sh"
chmod +x ./install.sh
./install.sh
```
